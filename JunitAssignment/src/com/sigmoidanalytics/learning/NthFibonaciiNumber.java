package com.sigmoidanalytics.learning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NthFibonaciiNumber  {

    /**
     * The function fib() is the main function of Geeks for Geeks that need to be evaluated.
     */

    // Approximate value of golden ratio
    private double PHI = 1.6180339;

    // Fibonacci numbers upto n = 5
    private int intialFibonaciiNumbers[] = { 0, 1, 1, 2, 3, 5 };



    /**
     * Here the fib functions return the Nth fibonacii Number, input parameter is the nth.
     * Where n denote
     * @param n
     * @return
     */
    public int fib (int n)
    {
        // Fibonacci numbers for n < 6 
        if (n < 6)
            return intialFibonaciiNumbers[n];

        // Else start counting from  
        // 5th term 
        int t = 5;
        int fn = 5;

        while (t < n) {
            fn = (int)Math.round(fn * PHI);
            t++;
        }
        return fn;
    }


    /**
     * Function ComputeForNumbers() is made just to get Some Numbers from a file "InputNumbers.txt"
     * And to collect the Output in "OutputNumbers.txt"
     *
     * file InputNumbers.txt => Path: /TDD/Junit/InputNumbers.txt
     * file OutputNumbers.txt => Path: /TDD/Junit/OutputNumbers.txt
     *
     * Basically both above files are used as a third party, so we don't actually use them.
     * There instances are used to for displaying the use of mock In "FibonaciiTests.java" In function "Using Mocks()"
     *
     */


    GetNumbers getNumObject  = new GetNumbers();

    public void ComputeForNumbers() throws IOException
    {
        List<Integer> inputList = getNumObject.GetList();
        List<Integer> outputList;
       outputList = new ArrayList<>();

       for(Integer i:inputList)
       {
           outputList.add(fib(i));
       }

       FileWriter writer = new FileWriter("OutputNumbers.txt");

       for(Integer i :outputList)
       {
           writer.write(String.valueOf(i)+"\n");
       }

    }



}
