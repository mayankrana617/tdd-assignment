package com.sigmoidanalytics.learning;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This is a Class used to replicate the use of Third Party.
 * The Third Party will use the GetList function defined below in GetNumbers class, to Get the all nth Fibonacii Numbers,
 * Where all the nth numbers are given in the "InputNumbers.txt" file and output is stored in "OutputNumbers.txt"
 *
 * file InputNumbers.txt => Path: /TDD/Junit/InputNumbers.txt
 * file OutputNumbers.txt => Path: /TDD/Junit/OutputNumbers.txt
 *
 *
 */
public class GetNumbers {

    public List GetList()throws IOException, FileNotFoundException
    {
        File obj = new File("InputNumbers.txt");
        List<Integer> inputList  = new ArrayList<>();

        Scanner scanner;

        scanner = new Scanner(obj);
        while (scanner.hasNextLine())
        {
            Integer intObject = Integer.parseInt(scanner.nextLine());
            inputList.add(intObject);
        }
        return inputList;
    }
}
