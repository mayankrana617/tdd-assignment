package com.sigmoidanalytics.learning;

import com.sigmoidanalytics.learning.NthFibonaciiNumber;
import jdk.jfr.Timespan;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * This is implementaion of a test class for a function "fib()" defined in "NthFibonaciiNunbers.java" file.
 */


public class FibonaciiTests {

    static NthFibonaciiNumber obj;


    // setup() is used to create a object of NthFibonaciiNumbers to test if the function fib (which is main function)
    // is working correctly.

    @BeforeClass
    public void  setup()
    {
         obj= new NthFibonaciiNumber();
    }
    

    //functions to test  base cases  which includes (0 and 1) fibonacii Numbers.
    @Test
    public void BaseTest()
    {
        assertEquals(0,obj.fib(0));
        assertEquals(1,obj.fib(1));
    }


    //It will test if the function is passing for small inputs like numbers 3
    @Test
    public  void smallNumberCorrect()
    {
        assertEquals(2, obj.fib(3));
    }


    // It is to check is incorrect numbers are also properly defined.
    @Test
    public void samllnumberIncorrect()
    {

        assertNotEquals(2,obj.fib(2));
    }



    // It is used to check is the main "fib()" function is showing all outputs within 500ms which is our timeout time.
    @Test(timeout = 500)
    public void LargeInputTimeRestriction()
    {
        assertEquals(317811,obj.fib(28));
    }


    /**
     * Now using mock and testing the use of third parties "(here file InputNumbers.txt)"
     */
    @Mock
    GetNumbers getNumbersMockObject;

    @Test
    public  void UsingMock() throws FileNotFoundException, IOException
    {

        MockitoAnnotations.initMocks(this);

        //This is a dummy list that is used in place of InputNumber.txt file.
        List<Integer> mocklist = new ArrayList();


        //adding 23 to dummy list
        mocklist.add(23);

        getNumbersMockObject.GetList();

        when(getNumbersMockObject.GetList()).thenReturn(mocklist);//this dummy list is returned here , in place of original list "InputNumbers.txt"
        verify(getNumbersMockObject,atLeastOnce()).GetList();
    }

}